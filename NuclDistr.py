#!/usr/bin/env python
from __future__ import division
import argparse
from Bio import SeqIO
from Bio.SeqIO import FastaIO
from collections import Counter

class NucleotideFreqs: 
    'Class to represent the frequencies of the nucleotides along the read lengths'
    def __init__(self):
        'The init'
        self.counts = {} #dict of Counters, one per position(key).

    def append(self, nucleotide_index, nucleotide):
        'It appends a value to the distribution corresponding to a category'
        try:
            cat_counts = self.counts[nucleotide_index]
        except KeyError:
            self.counts[nucleotide_index] = Counter() #create a Counter for this position.
            cat_counts = self.counts[nucleotide_index]
            cat_counts['A'] = 0
            cat_counts['T'] = 0
            cat_counts['G'] = 0
            cat_counts['C'] = 0
            cat_counts['N'] = 0
        nucleotide = nucleotide.upper()
        if nucleotide not in ('A', 'C', 'T', 'G'):
            nucleotide = 'N'
        cat_counts[nucleotide] += 1

    def print_freqs(self,upto_bp):
       'print the nucleotide frequencies for each position'
       print 'Nucleotide frequency per position'
       print '---------------------------------'
       pos = 0 
       for key in self.counts.keys():
          tot = sum(self.counts[key].values())
          nucls =  self.counts[key].keys()
          counts = self.counts[key].values()
          print key,'(',nucls[0],':',round(counts[0]/tot,2),',',           
          print nucls[1],':',round(counts[1]/tot,2),',',           
          print nucls[2],':',round(counts[2]/tot,2),',',           
          print nucls[3],':',round(counts[3]/tot,2),',',           
          print nucls[4],':',round(counts[4]/tot,2),')'           
          #print key,self.counts[key].keys(),self.counts[key].values(),sum(self.counts[key].values()) 
          pos += 1
          if pos>=upto_bp :
            break;
        

def title2ids(title):
    'It returns the id, name and description as a tuple.'
    'It takes the title of the FASTA file (without the beginning >)'
    items = title.strip().split()
    name = items[0]
    id_ = name
    if len(items) > 1:
        desc = ' '.join(items[1:])
    else:
        desc = ''
    return id_, name, desc

def _parse_arguments():
  description = 'Simple argument parser'
  parser = argparse.ArgumentParser(description=description,add_help=True)
  parser.add_argument('input_file',help='Sequence input file',type=argparse.FileType('rt'))
  #parser.add_argument('-o','--outfile', type=argparse.FileType('wt'),help="output filename",dest='outfile')  
  return parser

def calculate_stats(contig_lengths):
    'Calculate mean and variance of the contig lengths'
    if not contig_lengths:
        return None
    total_len = sum(contig_lengths)
    avg_len = total_len / len(contig_lengths)
    stdv = 0
    for c_len in contig_lengths:
        stdv += (c_len - avg_len)*(c_len - avg_len)
    stdv /= len(contig_lengths)
    return [total_len,avg_len,stdv]

def calculate_nx(contig_lengths, percentage):
    '''It calcalutes N50, N90 etc.
    Modified from wikipedia:
    Given a set of sequences of varying lengths, the N50 length is defined as
    the length N for which half of all bases in the sequences are in a sequence
    of length L >= N'''

    if not contig_lengths:
        return None
    total_len = sum(contig_lengths)
    len_desired = int(total_len * percentage / 100)
    contig_lengths.sort()
    contig_lengths.reverse()
    accum_len = 0
  
    for c_len in contig_lengths:
       accum_len += c_len    
       if accum_len >= len_desired:
            return c_len

# Main #
parser = _parse_arguments()  #create a parser
parsed_args = parser.parse_args()  #parse arguments, will open files also

#print 'You provided input file = ',parsed_args.input_file.name
#print 'You provided output file = ',parsed_args.outfile.name

#input is fasta file. Lets read it using biopython iterator for fasta files.
infile = parsed_args.input_file
seq_iter = FastaIO.FastaIterator(infile, title2ids) #iterator over objects of class 'SeqRecord'

nucl_freq = NucleotideFreqs()
contig_lengths = []
for sequence in seq_iter:
  #print 'object of class = ',sequence.__class__.__name__ #print the class name which should be 'SeqRecord'
  #print str(sequence.seq) #should print the sequence e.g. AGTTC
  #print list(enumerate(str(sequence.seq))) #should convert the sequence to and enumerate object e.g. [(0,'A'), (1,'G'), (2,'G'), (3,'A'), (4,'T')]
                                            #with an index and nucleotide
  for index,nucl in enumerate(str(sequence.seq)):
      nucl_freq.append(index,nucl)
  contig_lengths.append(len(sequence.seq))
  #print "==================="

#calculate N50 etc.
print 'Contig length statistics'
print '------------------------'
val = calculate_stats(contig_lengths)
print 'Total base pairs:',val[0]
print 'Total no. of contigs:',len(contig_lengths)
print 'Min contig length:',min(contig_lengths)
print 'Max contig length:',max(contig_lengths)
print 'Average contig length:',round(val[1],2)
print 'Variance of contig lengths:',round(val[2],2)
for nx in [25,50,75,90]:
  nx_vals = calculate_nx(contig_lengths, nx)
  print 'N'+str(nx)+":",nx_vals

#print the freqs of nucleotide for each base position.
nucl_freq.print_freqs(41)
infile.close()
